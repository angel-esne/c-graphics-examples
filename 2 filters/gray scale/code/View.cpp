
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.12

#include "View.hpp"
#include <color_filters.hpp>
#include <bitmap_loader.hpp>

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width),
        height      (height),
        pixel_buffer(width, height),
        blitter     (pixel_buffer)
    {
        bitmap = argb::load_bitmap< Pixel_Buffer::Color_Format > (image_path);

        if (bitmap)
        {
            apply_rgb_gray_scale_filter (*bitmap);
        }
    }

    void View::render ()
    {
        // Se borra el fondo de la pantalla con color blanco:

        pixel_buffer.clear (Color(1.f, 1.f, 1.f));

        // Se vuelca la imagen cargada en el buffer de pixels con transparencia y sin ella:

        blitter.blit (333, 144, *bitmap);

        // Se vuelca el buffer de pixels en la ventana:

        pixel_buffer.blit_to_window ();
    }

}
