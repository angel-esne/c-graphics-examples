
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.12 - 2021.04

#include <cassert>
#include <cmath>
#include "math.hpp"
#include "View.hpp"

namespace example
{
    
    const int View::vertices[][4] =
    {
        { -4, -4, +4, 1 },      // 0    // vértices del cubo
        { +4, -4, +4, 1 },      // 1
        { +4, +4, +4, 1 },      // 2
        { -4, +4, +4, 1 },      // 3
        { -4, -4, -4, 1 },      // 4
        { +4, -4, -4, 1 },      // 5
        { +4, +4, -4, 1 },      // 6
        { -4, +4, -4, 1 },      // 7
        { -5, -5,  0, 1 },      // 8    // vértices de los polígonos cortantes
        { +5, -5,  0, 1 },      // 9
        { +5, +5,  0, 1 },      // 10
        { -5, +5,  0, 1 },      // 11
        {  0, -5, +5, 1 },      // 12
        {  0, +5, +5, 1 },      // 13
        {  0, +5, -5, 1 },      // 14
        {  0, -5, -5, 1 },      // 15
    };
        
    const float View::colors[][3] =
    {
        { 1,   0,   0 },      // 0
        { 0,   1,   0 },      // 1
        { 0,   0,   1 },      // 2
        { 0,   0,   1 },      // 3
        { 1,   1,   0 },      // 4
        { 1,   0,   1 },      // 5
        { 1,   0,   0 },      // 6
        { 1,   1,   0 },      // 7
        { 1, .7f, .7f },      // 8
        { 1, .7f, .7f },      // 9
        { 1, .7f, .7f },      // 10
        { 1, .7f, .7f },      // 11
        { 1,   1, .9f },      // 12
        { 1,   1, .9f },      // 13
        { 1,   1, .9f },      // 14
        { 1,   1, .9f },      // 15
    };
        
    const int View::triangles[][3] =
    {
        {  0,  1,  2 },         // cube front
        {  0,  2,  3 },
        {  4,  0,  3 },         // cube left
        {  4,  3,  7 },
        {  5,  4,  7 },         // cube back
        {  5,  7,  6 },
        {  1,  5,  6 },         // cube right
        {  1,  6,  2 },
        {  3,  2,  6 },         // cube top
        {  3,  6,  7 },
        {  0,  4,  5 },         // cube bottom
        {  0,  5,  1 },
        {  8,  9, 10 },         // middle frontface
        {  8, 10, 11 },
        { 10,  9,  8 },         // middle backface
        { 11, 10,  8 },
        { 12, 13, 14 },         // middle leftface
        { 12, 14, 15 },
        { 14, 13, 12 },         // middle rightface
        { 15, 14, 12 },
    };

    View::View(unsigned width, unsigned height)
    :
        width       (width ),
        height      (height),
        color_buffer(width, height),
        rasterizer  (color_buffer )
    {
        // Se cargan en un búffer los datos del array:

        size_t number_of_vertices = sizeof(vertices) / sizeof(int) / 4;

        original_vertices.resize (number_of_vertices);

        for (size_t index = 0; index < number_of_vertices; index++)
        {
            original_vertices[index] = Vertex(vertices[index][0], vertices[index][1], vertices[index][2], vertices[index][3]);
        }

        transformed_vertices.resize (number_of_vertices);
            display_vertices.resize (number_of_vertices);

        // Se definen los datos de color de los vértices:

        size_t number_of_colors = sizeof(colors) / sizeof(int) / 3;

        assert(number_of_colors == number_of_vertices);             // Debe haber el mismo número
                                                                    // de colores que de vértices
        original_colors.resize (number_of_colors);

        for (size_t index = 0; index < number_of_colors; index++)
        {
            original_colors[index].set (colors[index][0], colors[index][1], colors[index][2]);
        }

        // Se generan los índices de los triángulos:

        size_t number_of_triangles = sizeof(triangles) / sizeof(int) / 3;

        original_indices.resize (number_of_triangles * 3);

        Index_Buffer::iterator indices_iterator = original_indices.begin ();

        for (size_t triangle_index = 0; triangle_index < number_of_triangles; triangle_index++)
        {
            *indices_iterator++ = triangles[triangle_index][0];
            *indices_iterator++ = triangles[triangle_index][1];
            *indices_iterator++ = triangles[triangle_index][2];
        }
    }

    void View::update ()
    {
        // Se actualizan los parámetros de transformatión (sólo se modifica el ángulo):

        static float angle = 0.f;

        angle += 0.025f;

        // Se crean las matrices de transformación:

        Matrix44 identity(1);
        Matrix44 scaling     = scale           (identity, 0.75f);
        Matrix44 rotation_x  = rotate_around_x (identity, -0.65f);
        Matrix44 rotation_y  = rotate_around_y (identity, angle);
        Matrix44 translation = translate       (identity, Vector3f{ 0, 0, -10 });
        Matrix44 projection  = perspective     (20, 1, 15, float(width) / height);

        // Creación de la matriz de transformación unificada:

        Matrix44 transformation = projection * translation * rotation_x * rotation_y * scaling;

        // Se transforman todos los vértices usando la matriz de transformación resultante:

        for (size_t index = 0, number_of_vertices = original_vertices.size (); index < number_of_vertices; index++)
        {
            // Se multiplican todos los vértices originales con la matriz de transformación y
            // se guarda el resultado en otro vertex buffer:

            Vertex & vertex = transformed_vertices[index] = transformation * original_vertices[index];

            // La matriz de proyección en perspectiva hace que el último componente del vector
            // transformado no tenga valor 1.0, por lo que hay que normalizarlo dividiendo:

            float divisor = 1.f / vertex[3];

            vertex[0] *= divisor;
            vertex[1] *= divisor;
            vertex[2] *= divisor;
            vertex[3]  = 1.f;
        }
    }

    void View::render ()
    {
        // Se convierten las coordenadas transformadas y proyectadas a coordenadas
        // de recorte (-1 a +1) en coordenadas de pantalla con el origen centrado.
        // La coordenada Z se escala a un valor suficientemente grande dentro del
        // rango de int (que es lo que espera fill_convex_polygon_z_buffer).

        Matrix44 identity(1);
        Matrix44 scaling        = scale (identity, float(width / 2), float(height / 2), 100000000.f);
        Matrix44 translation    = translate (identity, Vector3f{ float(width / 2), float(height / 2), 0.f });
        Matrix44 transformation = translation * scaling;

        for (size_t index = 0, number_of_vertices = transformed_vertices.size (); index < number_of_vertices; index++)
        {
            display_vertices[index] = Point4i( transformation * transformed_vertices[index] );
        }

        // Se borra el framebúffer y se dibujan los triángulos:

        rasterizer.clear ();

        for (int * indices = original_indices.data (), * end = indices + original_indices.size (); indices < end; indices += 3)
        {
            if (is_frontface (transformed_vertices.data (), indices))
            {
                // Se establece el color del polígono a partir del color de su primer vértice:

                rasterizer.set_color (original_colors[*indices]);

                // Se rellena el polígono:

                rasterizer.fill_convex_polygon_z_buffer (display_vertices.data (), indices, indices + 3);
            }
        }

        // Se copia el framebúffer oculto en el framebúffer de la ventana:

        color_buffer.blit_to_window ();
    }

    bool View::is_frontface (const Vertex * const projected_vertices, const int * const indices)
    {
        const Vertex & v0 = projected_vertices[indices[0]];
        const Vertex & v1 = projected_vertices[indices[1]];
        const Vertex & v2 = projected_vertices[indices[2]];

        // Se asumen coordenadas proyectadas y polígonos definidos en sentido horario.
        // Se comprueba a qué lado de la línea que pasa por v0 y v1 queda el punto v2:

        return ((v1[0] - v0[0]) * (v2[1] - v0[1]) - (v2[0] - v0[0]) * (v1[1] - v0[1]) > 0.f);
    }

}
