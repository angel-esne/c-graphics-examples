
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.12 - 2021.03

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <vector>
    #include <Point.hpp>
    #include "Rasterizer.hpp"
    #include <Color_Buffer.hpp>

    namespace example
    {

        using std::vector;
        using toolkit::Point4i;
        using toolkit::Point4f;

        class View
        {
        private:

            typedef argb::Rgb888     Color;
            typedef argb::Color_Buffer< Color > Color_Buffer;
            typedef Point4f          Vertex;
            typedef vector< Vertex > Vertex_Buffer;
            typedef vector< int    > Index_Buffer;
            typedef vector< Color  > Vertex_Colors;

        private:

            unsigned width;
            unsigned height;

            Color_Buffer               color_buffer;
            Rasterizer< Color_Buffer > rasterizer;

            Vertex_Buffer     original_vertices;
            Index_Buffer      original_indices;
            Vertex_Colors     original_colors;
            Vertex_Buffer     transformed_vertices;
            vector< Point4i > display_vertices;

        public:

            View(unsigned width, unsigned height);

            void update ();
            void render ();

        private:

            bool is_frontface (const Vertex * const projected_vertices, const int * const indices);

        };

    }

#endif
