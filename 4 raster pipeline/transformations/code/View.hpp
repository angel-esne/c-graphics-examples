
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.12 - 2021.03


#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <vector>
    #include <Point.hpp>
    #include "Rasterizer.hpp"
    #include <Color_Buffer.hpp>

    namespace example
    {

        class View
        {
        private:

            typedef argb::Color_Buffer< argb::Rgb888 > Color_Buffer;
            typedef toolkit::Point4f      Vertex;
            typedef std::vector< Vertex > Vertex_Buffer;
            typedef std::vector< int    > Index_Buffer;

        private:

            static const int  vertices[][4];
            static const int triangles[][3];

            unsigned width;
            unsigned height;

            Color_Buffer               color_buffer;
            Rasterizer< Color_Buffer > rasterizer;

            Vertex_Buffer original_vertices;
            Index_Buffer  original_indices;
            Vertex_Buffer transformed_vertices;

        public:

            View(unsigned width, unsigned height);

            void update ();
            void render ();

        };

    }

#endif
