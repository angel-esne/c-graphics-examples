
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.12

#include "View.hpp"
#include <color_filters.hpp>
#include <bitmap_loader.hpp>

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width ),
        height      (height),
        pixel_buffer(width, height),
        rasterizer  (pixel_buffer )
    {
    }

    void View::render ()
    {
        // Se borra el fondo de la pantalla con color blanco:

        pixel_buffer.clear  (Color(1, 1, 1));

        rasterizer.set_color      (0, 0, 1);
        rasterizer.draw_segment   (100, 100, 925, 670);
        rasterizer.draw_circle    (512, 384, 100);
        rasterizer.draw_ellipse   (262, 260, 500, 250);
        rasterizer.draw_rectangle (800,  50, 175,  75);
        rasterizer.fill_rectangle (700, 150, 175,  75);
        rasterizer.draw_triangle  (100, 668,  50, 518, 250, 718);
        rasterizer.fill_triangle  (200, 568, 150, 418, 350, 618);

        // Se vuelca el buffer de pixels en la ventana:

        pixel_buffer.blit_to_window ();
    }

}
