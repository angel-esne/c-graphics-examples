
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <memory>
    #include <string>
    #include <glm/glm.hpp>
    #include <Color_Buffer.hpp>
    #include "Rasterizer.hpp"

    namespace argb
    {

        class View
        {
        private:

            using Color_Format = Rgb565;
            using Color        = Color_Format;
            using Pixel_Buffer = Color_Buffer< Color_Format >;
            using Rasterizer   = Rasterizer< Pixel_Buffer >;

        private:

            unsigned     width;
            unsigned     height;

            Pixel_Buffer pixel_buffer;
            Rasterizer   rasterizer;

        public:

            View(unsigned width, unsigned height);

            void render ();

        };

    }

#endif
