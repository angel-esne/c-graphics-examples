
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2023.02

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include "math.hpp"
    #include <vector>
    #include <Color_Buffer.hpp>
    #include "Equilateral_Triangle.hpp"

    namespace argb
    {

        class View
        {
        private:

            using Color_Format = Rgb24;
            using Color        = Color_Format;
            using Pixel_Buffer = Color_Buffer< Color_Format >;

        private:

            static constexpr unsigned points_per_frame = 10;

            unsigned     width;
            unsigned     height;
            Pixel_Buffer pixel_buffer;

            std::mt19937 prng;                      // pseudo random number generator
            const Color  color{ 1, 1, 1 };

            vec2         current_point;             // point inside the triangle which position evolves each frame

            Equilateral_Triangle triangle;

        public:

            View(unsigned width, unsigned height);

            void update ();
            void render ();

        private:

            void draw_triangle_vertices ();

            vec2 random_point_inside_the_triangle ();

            real random_value_between_0_and_1 ()
            {
                return real(prng ()) / real(std::numeric_limits< decltype(prng)::result_type >::max ());
            }

        };

    }

#endif
