
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2023.02

#ifndef MATH_HEADER
#define MATH_HEADER

    #include <cmath>
    #include <glm/glm.hpp>
    #include <limits>
    #include <random>

    namespace argb
    {

        using real = double;
        using vec2 = glm::vec< 2, real, glm::highp >;

    }

#endif
