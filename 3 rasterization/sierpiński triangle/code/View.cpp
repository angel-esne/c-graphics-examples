
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2023.02

#include <chrono>
#include <cstdlib>
#include "View.hpp"

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width ),
        height      (height),
        pixel_buffer(width, height),
        prng        (unsigned(std::chrono::system_clock::now ().time_since_epoch ().count ())),
        triangle    (vec2{ real(width) / 2, real(height) / 2 }, real(150))
    {
        pixel_buffer.clear (Color{ 0, 0, 0 });

        draw_triangle_vertices ();

        current_point = random_point_inside_the_triangle ();
    }

    void View::update ()
    {
        for (unsigned i = 0; i < points_per_frame; ++i)
        {
            current_point = (current_point + triangle.vertex (prng () % 3)) * real(0.5);

            pixel_buffer.set_pixel
            (
                int(round (current_point.x)),
                int(round (current_point.y)),
                color
            );
        }
    }

    void View::render ()
    {
        pixel_buffer.blit_to_window ();
    }

    void View::draw_triangle_vertices ()
    {
        for (unsigned index = 0; index < 3; ++index)
        {
            const vec2 & vertex = triangle.vertex (index);

            for (int r = -1; r < 2; ++r)
            {
                pixel_buffer.set_pixel (int(vertex.x) + r, int(vertex.y) - 1, color);
                pixel_buffer.set_pixel (int(vertex.x) + r, int(vertex.y) + 1, color);
                pixel_buffer.set_pixel (int(vertex.x) - 1, int(vertex.y) + r, color);
                pixel_buffer.set_pixel (int(vertex.x) + 1, int(vertex.y) + r, color);
            }
        }
    }

    vec2 View::random_point_inside_the_triangle ()
    {
        real u = random_value_between_0_and_1 ();
        real v = random_value_between_0_and_1 ();

        const vec2 & top   = triangle.  top_vertex ();
        const vec2 & left  = triangle. left_vertex ();
        const vec2 & right = triangle.right_vertex ();

        vec2 horizontal_line_point_a = top + (left  - top) * v;
        vec2 horizontal_line_point_b = top + (right - top) * v;

        return horizontal_line_point_a * u + horizontal_line_point_b * (real(1) - u);
    }

}
