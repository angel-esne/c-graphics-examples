
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2023.02

#ifndef EQUILATERAL_TRIANGLE_HEADER
#define EQUILATERAL_TRIANGLE_HEADER

    #include "math.hpp"

    namespace argb
    {
			
		class Equilateral_Triangle
        {
        private:
                
            vec2 vertices[3];

        public:

            Equilateral_Triangle(const vec2 & center, real apothem)
            {
                real h = apothem / sin (to_radians (30));
                real x =       h * sin (to_radians (60));
                vertices[0] = center + vec2{ +x, apothem };
                vertices[1] = center + vec2{ -x, apothem };
                vertices[2] = { center.x, center.y - h };
            }

            vec2 & vertex (unsigned index)
            {
                return vertices[index];
            }

            vec2 &   top_vertex () { return vertices[2]; }
            vec2 &  left_vertex () { return vertices[1]; }
            vec2 & right_vertex () { return vertices[0]; }

        private:

            static constexpr real to_radians (real degrees)
            {
                return degrees * real(3.1415926535897932384626433) / real(180);
            }

        };

    }

#endif
