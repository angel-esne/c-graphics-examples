
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2016.11+

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <vector>
    #include <Color_Buffer.hpp>

    namespace argb
    {

        class View
        {
        private:

            using Color_Format = Rgb24;
            using Color        = Color_Format;
            using Pixel_Buffer = Color_Buffer< Color_Format >;

			struct Field
			{
				float energy;

				float center_x;
				float center_y;

                float speed_x;
                float speed_y;

				float potential_at (const float & x, const float & y) const
	            {
		            float  delta_x = center_x - x;
		            float  delta_y = center_y - y;

		            return energy / (delta_x * delta_x + delta_y * delta_y);
	            }
			};

            static constexpr size_t number_of_fields  = 20;
            static constexpr int    field_energy_base = 400;

        private:

            unsigned     width;
            unsigned     height;
            Pixel_Buffer pixel_buffer;
			
			std::vector< Field > fields;

        public:

            View(unsigned width, unsigned height);

            void update ();
            void render ();

        };

    }

#endif
