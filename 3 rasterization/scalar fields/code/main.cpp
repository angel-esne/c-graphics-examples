
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#include "View.hpp"
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

using namespace sf;
using namespace argb;

int main ()
{
    constexpr unsigned window_width  = 1024;
    constexpr unsigned window_height =  768;

    // Create the window and the view that will be shown within the window:

    Window window(VideoMode(window_width, window_height), "Scalar field rasterization");
    View   view  (window_width, window_height);

    window.setVerticalSyncEnabled (false);

    // Run the main loop:

    bool running = true;

    do
    {
        // Attend the window events:

        Event event;

        while (window.pollEvent (event))
        {
            if (event.type == Event::Closed)
            {
                running = false;
            }
        }

        // Update and repaint the view:

        view.update ();
        view.render ();

        // Swap the OpenGL buffers:

        window.display ();
    }
    while (running);

    return EXIT_SUCCESS;
}
