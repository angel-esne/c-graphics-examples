
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#include <algorithm>
#include <SOIL2.h>
#include "View.hpp"

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width ),
        height      (height),
        pixel_buffer(width, height)
    {
        int image_width    = 0;
        int image_height   = 0;
        int image_channels = 0;

        unsigned char * loaded_pixels = SOIL_load_image
        (
             image_path,
            &image_width, 
            &image_height, 
            &image_channels,
             SOIL_LOAD_RGB              // Indica que nos devuelva los pixels en formato RGB24
        );                              // al margen del formato usado en el archivo

        // Si loaded_pixels no es nullptr la imagen se ha podido cargar correctamente:

        if (loaded_pixels)
        {
            // Solo se copia la imagen en pixel_buffer si tienen el mismo tamaño:

            if (image_width == pixel_buffer.get_width () && image_height == pixel_buffer.get_height ())
            {
                // Se determina cómo copiar los pixels dependiendo del formato de destino:

                if (Pixel_Buffer::Color::format_id == Rgb24::format_id)
                {
                    // Se copian los bytes directamente (de formato Rgb24 a formato Rgb24):

                    std::copy_n
                    (
                        loaded_pixels, 
                        size_t(image_width) * image_height * 3,
                        reinterpret_cast< unsigned char * >(pixel_buffer.pixels ())
                    );
                }
                else
                if (Pixel_Buffer::Color::format_id == Rgb565::format_id)
                {
                    // Se copian los pixels convirtiéndolos de formato Rgb24 a formato Rgb565:

                    this->copy
                    (
                        reinterpret_cast< Rgb24  * >(loaded_pixels),
                        reinterpret_cast< Rgb565 * >(pixel_buffer.pixels ()),
                        size_t(image_width) * image_height
                    );
                }
            }

            // Se libera la memoria que reservó SOIL2 para cargar la imagen:

            SOIL_free_image_data (loaded_pixels);
        }
    }

    void View::render ()
    {
        pixel_buffer.blit_to_window ();
    }

    void View::copy (Rgb24 * source, Rgb565 * target, size_t count)
    {
        while (count--)
        {
            *target++ = convert_to_rgb565 (*source++);
        }
    }

}
