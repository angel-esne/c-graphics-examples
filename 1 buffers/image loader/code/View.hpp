
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#ifndef ARGB_VIEW_HEADER
#define ARGB_VIEW_HEADER

    #include <Color_Buffer.hpp>

    namespace argb
    {

        class View
        {
        private:

            typedef Color_Buffer< Rgb565 > Pixel_Buffer;

        private:

            static constexpr const char image_path[] = "../../assets/landscape-800x600.jpg";

            unsigned     width;
            unsigned     height;
            Pixel_Buffer pixel_buffer;

        public:

            View(unsigned width, unsigned height);

            void render ();

        private:

            Rgb565 convert_to_rgb565 (const Rgb24 & source)
            {
                return
                    (uint16_t(source.red   ()) >> 3 << 11) |
                    (uint16_t(source.green ()) >> 2 <<  5) |
                    (uint16_t(source.blue  ()) >> 3      );
            }

            void copy (Rgb24 * source, Rgb565 * target, size_t count);

        };

    }

#endif
