
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.12

#include <algorithm>
#include <SOIL2.h>
#include "View.hpp"
#include <bitmap_loader.hpp>

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width),
        height      (height),
        pixel_buffer(width, height),
        blitter     (pixel_buffer)
    {
        bitmap = argb::load_bitmap< Color_Format > (image_path);
    }

    void View::render ()
    {
        // Se borra el fondo de la pantalla con color blanco:

        pixel_buffer.clear ({ 1.f, 1.f, 1.f });

        // Se vuelca la imagen cargada en el buffer de pixels con transparencia y sin ella:

        blitter.blit (175, 75, *bitmap);
        blitter.blit< blend_half > (475, 175, *bitmap);

        // Se vuelca el buffer de pixels en la ventana:

        pixel_buffer.blit_to_window ();
    }

}
