
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.12

#include <algorithm>
#include <bitmap_loader.hpp>
#include "View.hpp"

namespace argb
{

    View::View(unsigned width, unsigned height)
    :
        width       (width),
        height      (height),
        pixel_buffer(width, height),
        blitter     (pixel_buffer),
        position    (width / 2, height / 2),
        speed       (5, 5)
    {
        bitmap = argb::load_bitmap< Pixel_Buffer::Color_Format > (image_path);
    }

    void View::render ()
    {
        // Se borra el fondo de la pantalla:

        pixel_buffer.clear (Color(1.f, 1.f, 1.f));

        // Se actualiza la posición de la imagen que se va a dibujar:

        position += speed;

        if (position.x <  0)
        {
            position.x =  0;
            speed.x    = -speed.x;
        }
        else
        if (position.x +  bitmap->get_width  () > width)
        {
            position.x =  width - bitmap->get_width ();
            speed.x    = -speed.x;
        }

        if (position.y <  0)
        {
            position.y =  0;
            speed.y    = -speed.y;
        }
        else
        if (position.y +  bitmap->get_height () > height)
        {
            position.y =  height - bitmap->get_height ();
            speed.y    = -speed.y;
        }

        // Se vuelca la imagen cargada en el buffer de pixels:

        blitter.blit (unsigned(position.x), unsigned(position.y), *bitmap);

        // Se vuelca el buffer de pixels en la ventana:

        pixel_buffer.blit_to_window ();
    }

}
