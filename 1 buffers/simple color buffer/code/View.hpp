
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#ifndef VIEW_HEADER
#define VIEW_HEADER

    #include <Color_Buffer.hpp>

    namespace argb
    {

        class View
        {
        private:

            typedef Color_Buffer< Rgb565 > Pixel_Buffer;

        private:

            unsigned     width;
            unsigned     height;
            Pixel_Buffer pixel_buffer;

        public:

            View(unsigned width, unsigned height)
            :
                width       (width ),
                height      (height),
                pixel_buffer(width, height)
            {
            }

            void paint ();

        };

    }

#endif
