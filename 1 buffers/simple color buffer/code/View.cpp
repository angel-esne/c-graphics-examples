
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2013.11 - 2020.12

#include "View.hpp"

namespace argb
{

    void View::paint ()
    {
        // Draws a yellow diagonal line:

        Pixel_Buffer::Color color(1.f, 1.f, 0.f);

        for (unsigned i = 0, max = width < height ? width : height; i < max; i++)
        {
            pixel_buffer.set_pixel (i, i, color);
        }

        // Draws a cyan diagonal line:

        color.set (0.f, 1.f, 1.f);

        for (unsigned i = 0, max = width < height ? width : height; i < max; i++)
        {
            pixel_buffer.set_pixel (width - 1 - i, i, color);
        }

        // Moves the pixel buffer into the window:

        pixel_buffer.blit_to_window ();
    }

}
