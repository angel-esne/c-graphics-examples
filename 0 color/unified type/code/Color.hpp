
// Código bajo licencia Boost Software License, version 1.0
// Ver www.boost.org/LICENSE_1_0.txt
// angel.rodriguez@esne.edu
// 2020.12

#include <cstdint>

namespace argb
{
    
    // Formatos de píxel desempaquetados (cada componente se guarda en una variable independiente):

    class Xyz_Layout
    {
    public:

        enum Components : unsigned
        {
            X, Y, Z
        };
        
        static constexpr unsigned size = 3;
    };

    class Rgb_Layout
    {
    public:

        enum Components : unsigned
        {
            RED   = 0,
            GREEN = 1,
            BLUE  = 2,
            R     = RED,
            G     = GREEN,
            B     = BLUE
        };
        
        static constexpr unsigned size = 3;
    };
    
    class Bgr_Layout
    {
    public:

        enum Components : unsigned
        {
            BLUE  = 0,
            GREEN = 1,
            RED   = 2,
            B     = BLUE,
            G     = GREEN,
            R     = RED
        };
        
        static constexpr unsigned size = 3;
    };

    // ------------------------------------------------------------------------------------------ //

    // SERÍA INTERESANTE METER AQUÍ EL COMPOSITE TYPE PARA ARGB32... SOLO POR ESE CASO PODRÍA MERECER LA PENA.

    template< class COMPONENT_LAYOUT, typename COMPONENT_TYPE >
    struct Color : public COMPONENT_LAYOUT
    {
        using Component_Type   = COMPONENT_TYPE;
        using Component_Layout = COMPONENT_LAYOUT;

        Component_Type components[Component_Layout::size];
    };

    // ------------------------------------------------------------------------------------------ //

    template< class COMPONENT_LAYOUT, typename COMPONENT_TYPE >
    struct Additive_Primaries : public Color< COMPONENT_LAYOUT, COMPONENT_TYPE >
    {
        using          Color = Color< COMPONENT_LAYOUT, COMPONENT_TYPE >;
        using typename Color::Component_Type;
        using typename Color::Component_Layout::Components;
        using          Color::components;

        Component_Type & r     ()       { return components[Components::R    ]; }
        Component_Type   r     () const { return components[Components::R    ]; }
        Component_Type & red   ()       { return components[Components::RED  ]; }
        Component_Type   red   () const { return components[Components::RED  ]; }

        Component_Type & g     ()       { return components[Components::G    ]; }
        Component_Type   g     () const { return components[Components::G    ]; }
        Component_Type & green ()       { return components[Components::GREEN]; }
        Component_Type   green () const { return components[Components::GREEN]; }

        Component_Type & b     ()       { return components[Components::B    ]; }
        Component_Type   b     () const { return components[Components::B    ]; }
        Component_Type & blue  ()       { return components[Components::BLUE ]; }
        Component_Type   blue  () const { return components[Components::BLUE ]; }
    };

    // Formatos de píxel no empaquetados (los componentes son accesibles por separado):

    template< typename COMPONENT_TYPE >
    using Rgb    = Additive_Primaries< Rgb_Layout, COMPONENT_TYPE >;

    using Rgbf   = Rgb< float   >;
    using Rgb888 = Rgb< uint8_t >;
    using Rgb24  = Rgb888;

    template< typename COMPONENT_TYPE >
    using Bgr    = Additive_Primaries< Bgr_Layout, COMPONENT_TYPE >;

    using Bgrf   = Bgr< float   >;
    using Bgr888 = Bgr< uint8_t >;
    using Bgr24  = Bgr888;
    
    // ------------------------------------------------------------------------------------------ //

    template< unsigned BIT_COUNT >
    struct Composite_Type;

    template< > struct Composite_Type<  8 > { using Type = uint8_t;  };
    template< > struct Composite_Type< 16 > { using Type = uint16_t; };
    template< > struct Composite_Type< 32 > { using Type = uint32_t; };
    template< > struct Composite_Type< 64 > { using Type = uint64_t; };
    
    // ------------------------------------------------------------------------------------------ //

    template< unsigned RED_BITS, unsigned GREEN_BITS, unsigned BLUE_BITS >
    class Packed_Rgb_Layout
    {
    public:

        static constexpr unsigned r_bits    =   RED_BITS;
        static constexpr unsigned g_bits    = GREEN_BITS;
        static constexpr unsigned b_bits    =  BLUE_BITS;

        static constexpr unsigned r_shift   = g_bits + b_bits;
        static constexpr unsigned g_shift   = b_bits;
        static constexpr unsigned b_shift   = 0;

        static constexpr unsigned bit_count = r_bits + g_bits + b_bits;

        using  Composite_Type = typename Composite_Type< bit_count >::Type;

        static constexpr Composite_Type r_mask = (1 << r_bits) - 1;
        static constexpr Composite_Type g_mask = (1 << g_bits) - 1;
        static constexpr Composite_Type b_mask = (1 << b_bits) - 1;

    };

    // ------------------------------------------------------------------------------------------ //

    // ESTARÍA GENIAL SI FUESE POSIBLE UNIFICAR COLOR Y PACKED_COLOR VOLVIENDO A LA IDEA DE LA UNIÓN
    // DE COMPONENTES Y VALUE.

    template< class COMPONENT_LAYOUT >
    struct Packed_Color : public COMPONENT_LAYOUT
    {
        using          Component_Layout = COMPONENT_LAYOUT;
        using typename Component_Layout::Composite_Type;

        Composite_Type value;
    };

    // ------------------------------------------------------------------------------------------ //

    template< class PACKED_COMPONENT_LAYOUT >
    struct Packed_Additive_Primaries : public Packed_Color< PACKED_COMPONENT_LAYOUT >
    {
        using          Color = Packed_Color< PACKED_COMPONENT_LAYOUT >;
        using typename Color::Component_Layout::Composite_Type;
        using          Color::value;

        using          Color::r_shift;
        using          Color::r_mask;
        using          Color::g_shift;
        using          Color::g_mask;
        using          Color::b_shift;
        using          Color::b_mask;

        Composite_Type r     () const { return (value >> r_shift) & r_mask; }
        Composite_Type red   () const { return (value >> r_shift) & r_mask; }

        Composite_Type g     () const { return (value >> g_shift) & g_mask; }
        Composite_Type green () const { return (value >> g_shift) & g_mask; }

        Composite_Type b     () const { return (value >> b_shift) & b_mask; }
        Composite_Type blue  () const { return (value >> b_shift) & b_mask; }
    };

    // Formatos de píxel empaquetados (los componentes están mezclados en una sola variable):
    // Con estos tipos de formatos de píxel no se tiene en cuenta el endianness.
    
    using Rgb332 = Packed_Additive_Primaries< Packed_Rgb_Layout< 3, 3, 2 > >;
    using Rgb565 = Packed_Additive_Primaries< Packed_Rgb_Layout< 5, 6, 5 > >;
    
}
