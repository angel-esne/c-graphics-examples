
// Este código es de dominio público.
// angel.rodriguez@esne.edu
// 2020.12

#include "Color.hpp"
#include <type_traits>

using namespace argb;

int main ()
{
    static_assert(std::is_pod< Rgb888 >::value, "Se espera que Rgb888 sea POD.");

    Rgb888 a;

    a.red () = 0;

    return 0;
}
